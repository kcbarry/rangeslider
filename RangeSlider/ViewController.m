//
//  ViewController.m
//  RangeSlider
//
//  Created by Clark Barry on 9/22/12.
//  Copyright (c) 2012 CSHaus. All rights reserved.
//

#import "ViewController.h"
#import "RangeSlider.h"
@interface ViewController ()

@end

@implementation ViewController

-(void)loadView{
    [super loadView];
    self.view.backgroundColor = [UIColor whiteColor];
    RangeSlider *slider = [[RangeSlider alloc]initWithFrame:CGRectMake(5, 100, 250, 50)];
    [slider setLabelProperty:RangeSliderLabelShowWhenUpdating];
    slider.minimumValue = 0;
    slider.maximumValue = 100;
    [self.view addSubview:slider];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
